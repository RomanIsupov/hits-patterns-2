package com.romanisupov.core.enumeration;

import lombok.Getter;

@Getter
public enum OperationType {

    OPEN_ACCOUNT("Открытие счета"),

    TOP_UP("Пополнение счета"),

    WITHDRAW("Снятие со счета"),

    CLOSE_ACCOUNT("Закрытие счета"),

    OPEN_CREDIT_ACCOUNT("Взятие кредита"),

    CLOSE_CREDIT_ACCOUNT("Погашение кредита");


    private String name;

    OperationType(String name) {
        this.name = name;
    }
}
