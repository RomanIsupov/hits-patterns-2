package com.romanisupov.core.enumeration;

public enum AccountType {

    CREDIT,

    DEBIT
}
