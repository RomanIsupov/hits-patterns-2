package com.romanisupov.core.service;

import com.romanisupov.core.controller.dto.OperationsHistoryDto;
import com.romanisupov.core.entity.OperationsHistory;
import com.romanisupov.core.enumeration.AccountType;
import com.romanisupov.core.repository.OperationsHistoryRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationsHistoryService {

    private OperationsHistoryRepository operationsHistoryRepository;

    @Autowired
    public void setOperationsHistoryRepository(
        OperationsHistoryRepository operationsHistoryRepository) {
        this.operationsHistoryRepository = operationsHistoryRepository;
    }

    private OperationsHistoryDto entityToDto(OperationsHistory operationsHistory) {
        Long accountId = operationsHistory.getCreditAccountId() == null ? operationsHistory.getDebitAccountId()
            : operationsHistory.getCreditAccountId();
        return new OperationsHistoryDto(
            operationsHistory.getId(),
            operationsHistory.getOperationType().getName(),
            accountId,
            operationsHistory.getAmountOfMoney(),
            operationsHistory.getTimestamp()
        );
    }

    public List<OperationsHistoryDto> getOperationsHistory(AccountType accountType, Long accountId) {
        if (accountType == AccountType.DEBIT) {
            return operationsHistoryRepository.findAllByDebitAccountId(accountId).stream()
                .map(this::entityToDto).toList();
        } else {
            return operationsHistoryRepository.findAllByCreditAccountId(accountId).stream()
                .map(this::entityToDto).toList();
        }
    }
}
