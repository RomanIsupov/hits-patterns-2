package com.romanisupov.core.service;

import com.romanisupov.core.controller.dto.AccountRequestDto;
import com.romanisupov.core.controller.dto.AccountResponseDto;
import com.romanisupov.core.entity.Account;
import com.romanisupov.core.entity.CreditAccount;
import com.romanisupov.core.entity.DebitAccount;
import com.romanisupov.core.enumeration.AccountType;
import com.romanisupov.core.repository.ClientRepository;
import com.romanisupov.core.repository.CreditAccountRepository;
import com.romanisupov.core.repository.CurrencyRepository;
import com.romanisupov.core.repository.DebitAccountRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private DebitAccountRepository debitAccountRepository;

    private CreditAccountRepository creditAccountRepository;

    private CurrencyRepository currencyRepository;

    private ClientRepository clientRepository;

    @Autowired
    public void setDebitAccountRepository(DebitAccountRepository debitAccountRepository) {
        this.debitAccountRepository = debitAccountRepository;
    }

    @Autowired
    public void setCreditAccountRepository(CreditAccountRepository creditAccountRepository) {
        this.creditAccountRepository = creditAccountRepository;
    }

    @Autowired
    public void setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }


    private AccountResponseDto entityToDto(Account account) {
        return new AccountResponseDto(
            account.getId(),
            account.getCurrency().getName(),
            account.getBalance(),
            account.getClient().getPassportId(),
            account.getIsClosed()
        );
    }

    //todo add exceptions
    private DebitAccount dtoToDebitAccount(AccountRequestDto dto) {
        DebitAccount debitAccount = new DebitAccount();
        debitAccount.setCurrency(currencyRepository.findByName(dto.getCurrency()).orElseThrow());
        debitAccount.setBalance(dto.getBalance());
        debitAccount.setClient(clientRepository.findByPassportId(dto.getPassportId()).orElseThrow());
        debitAccount.setIsClosed(false);
        return debitAccount;
    }

    //todo add exceptions
    private CreditAccount dtoToCreditAccount(AccountRequestDto dto) {
        CreditAccount creditAccount = new CreditAccount();
        creditAccount.setCurrency(currencyRepository.findByName(dto.getCurrency()).orElseThrow());
        creditAccount.setBalance(dto.getBalance());
        creditAccount.setClient(clientRepository.findByPassportId(dto.getPassportId()).orElseThrow());
        creditAccount.setIsClosed(false);
        return creditAccount;
    }

    public List<AccountResponseDto> getAllAccounts(String passportId) {
        List<Account> accounts = new ArrayList<>();
        debitAccountRepository.findAll().forEach(accounts::add);
        creditAccountRepository.findAll().forEach(accounts::add);
        return accounts.stream().map(this::entityToDto).toList();
    }

    public AccountResponseDto openAccount(AccountType accountType, AccountRequestDto accountRequestDto) {
        Account newAccount;
        if (accountType == AccountType.DEBIT) {
            newAccount = debitAccountRepository.save(dtoToDebitAccount(accountRequestDto));
        } else {
            newAccount = creditAccountRepository.save(dtoToCreditAccount(accountRequestDto));
        }
        return entityToDto(newAccount);
    }

    public void closeAccount(AccountType accountType, Long accountId) {
        if (accountType == AccountType.DEBIT) {
            debitAccountRepository.closeAccountById(accountId);
        } else {
            creditAccountRepository.closeAccountById(accountId);
        }
    }

    //todo exceptions
    public void changeBalance(AccountType accountType, Long accountId, Long changeValue) {
        if (accountType == AccountType.DEBIT
            && validateBalance(debitAccountRepository.getBalance(accountId).orElseThrow(), changeValue)) {
            debitAccountRepository.changeBalance(accountId, changeValue);
        } else if (accountType == AccountType.CREDIT
            && validateBalance(creditAccountRepository.getBalance(accountId).orElseThrow(), changeValue)) {
            creditAccountRepository.changeBalance(accountId, changeValue);
        }
    }

    private boolean validateBalance(Long currentBalance, Long changeBalance) {
        if (currentBalance + changeBalance < 0) {
            return true;
        }
        return false;
    }
}
