package com.romanisupov.core.service;

import com.romanisupov.core.entity.Client;
import com.romanisupov.core.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    private ClientRepository clientRepository;

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public void addClient(String passportId) {
        Client newClient = new Client();
        newClient.setPassportId(passportId);
        clientRepository.save(newClient);
    }
}
