package com.romanisupov.core.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountResponseDto {

    private Long id;

    private String currency;

    private Long balance;

    private String passportId;

    private Boolean isClosed;
}
