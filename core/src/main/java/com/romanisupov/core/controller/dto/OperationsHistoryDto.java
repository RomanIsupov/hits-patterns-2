package com.romanisupov.core.controller.dto;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OperationsHistoryDto {

    private Long id;

    private String operationType;

    private Long accountId;

    private Long amountOfMoney;

    private LocalDateTime timestamp;
}
