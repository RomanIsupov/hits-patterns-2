package com.romanisupov.core.controller;

import com.romanisupov.core.controller.dto.AccountRequestDto;
import com.romanisupov.core.controller.dto.AccountResponseDto;
import com.romanisupov.core.enumeration.AccountType;
import com.romanisupov.core.service.AccountService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public ResponseEntity<List<AccountResponseDto>> getAllAccounts(@RequestParam String passportId) {
        return ResponseEntity.ok(accountService.getAllAccounts(passportId));
    }

    @PostMapping("/debit")
    public ResponseEntity<AccountResponseDto> openDebitAccount(@RequestBody AccountRequestDto accountRequestDto) {
        return ResponseEntity.ok(accountService.openAccount(AccountType.DEBIT, accountRequestDto));
    }

    @PostMapping("/credit")
    public ResponseEntity<AccountResponseDto> openCreditAccount(@RequestBody AccountRequestDto accountRequestDto) {
        return ResponseEntity.ok(accountService.openAccount(AccountType.CREDIT, accountRequestDto));
    }

    @DeleteMapping("/debit")
    public ResponseEntity<Void> closeDebitAccount(@RequestParam Long accountId) {
        accountService.closeAccount(AccountType.DEBIT, accountId);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/credit")
    public ResponseEntity<Void> closeCreditAccount(@RequestParam Long accountId) {
        accountService.closeAccount(AccountType.CREDIT, accountId);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/debit")
    public ResponseEntity<Void> changeBalanceDebitAccount(
        @RequestParam Long accountId,
        @RequestParam Long changeValue) {
        accountService.changeBalance(AccountType.DEBIT, accountId, changeValue);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/credit")
    public ResponseEntity<Void> changeBalanceCreditAccount(
        @RequestParam Long accountId,
        @RequestParam Long changeValue) {
        accountService.changeBalance(AccountType.CREDIT, accountId, changeValue);
        return ResponseEntity.ok().build();
    }
}
