package com.romanisupov.core.controller;

import com.romanisupov.core.controller.dto.OperationsHistoryDto;
import com.romanisupov.core.enumeration.AccountType;
import com.romanisupov.core.service.OperationsHistoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/operations")
public class OperationsHistoryController {

    private OperationsHistoryService operationsHistoryService;

    @Autowired
    public void setOperationsHistoryService(OperationsHistoryService operationsHistoryService) {
        this.operationsHistoryService = operationsHistoryService;
    }

    @GetMapping("/debit")
    public ResponseEntity<List<OperationsHistoryDto>> getDebitAccountOperationsHistory(
        @RequestParam Long accountId) {
        return ResponseEntity.ok(operationsHistoryService.getOperationsHistory(AccountType.DEBIT, accountId));
    }

    @GetMapping("/credit")
    public ResponseEntity<List<OperationsHistoryDto>> getCreditAccountOperationsHistory(
        @RequestParam Long accountId) {
        return ResponseEntity.ok(operationsHistoryService.getOperationsHistory(AccountType.CREDIT, accountId));
    }
}
