package com.romanisupov.core.repository;

import com.romanisupov.core.entity.CreditAccount;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditAccountRepository extends CrudRepository<CreditAccount, Long> {

    @Query("update CreditAccount set isClosed = false where id = :accountId")
    void closeAccountById(@Param("accountId") Long accountId);

    @Query("select balance from CreditAccount where id = :accountId")
    Optional<Long> getBalance(@Param("accountId") Long accountId);

    @Query("update CreditAccount set balance = balance + :balanceChange where id = :accountId")
    void changeBalance(@Param("accountId") Long accountId, @Param("balanceChange") Long balanceChange);
}
