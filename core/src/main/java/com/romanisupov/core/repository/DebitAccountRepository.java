package com.romanisupov.core.repository;

import com.romanisupov.core.entity.DebitAccount;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DebitAccountRepository extends CrudRepository<DebitAccount, Long> {

    @Query("update DebitAccount set isClosed = false where id = :accountId")
    void closeAccountById(@Param("accountId") Long accountId);

    @Query("select balance from DebitAccount where id = :accountId")
    Optional<Long> getBalance(@Param("accountId") Long accountId);

    @Query("update DebitAccount set balance = balance + :balanceChange where id = :accountId")
    void changeBalance(@Param("accountId") Long accountId, @Param("balanceChange") Long balanceChange);
}
