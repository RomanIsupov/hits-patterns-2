package com.romanisupov.core.repository;

import com.romanisupov.core.entity.Client;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    Optional<Client> findByPassportId(String passportId);
}
