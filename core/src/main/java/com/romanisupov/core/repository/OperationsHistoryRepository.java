package com.romanisupov.core.repository;

import com.romanisupov.core.entity.OperationsHistory;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationsHistoryRepository extends CrudRepository<OperationsHistory, Long> {

    List<OperationsHistory> findAllByDebitAccountId(Long debitAccountId);

    List<OperationsHistory> findAllByCreditAccountId(Long creditAccountId);
}
