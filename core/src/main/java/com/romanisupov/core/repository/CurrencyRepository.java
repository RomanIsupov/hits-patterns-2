package com.romanisupov.core.repository;

import com.romanisupov.core.entity.Currency;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Long> {

    Optional<Currency> findByName(String name);
}
