package com.romanisupov.core.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "credit_accounts")
@Getter
@Setter
@AllArgsConstructor
public class CreditAccount extends Account {
}
